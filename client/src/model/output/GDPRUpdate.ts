import {CustomerUpdate, Type} from "./CustomerUpdate";

export interface GDPRUpdate extends CustomerUpdate {

}

export function create(): GDPRUpdate {
  return {
    __type__: Type.GDPR,
  }
}
