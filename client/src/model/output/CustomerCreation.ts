import {AdminCreation} from "./AdminCreation";

export interface CustomerCreation extends AdminCreation {
  firstName: string
  lastName: string
  birthDate: string
  address: string
  city: string
  zipCode: string
}
