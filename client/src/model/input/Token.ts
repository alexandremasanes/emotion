import {User} from "./User";

export interface Token {
  value: string
  expirationTime: {seconds:number, nanos:number}
  user: User
  persistent?: boolean
}
