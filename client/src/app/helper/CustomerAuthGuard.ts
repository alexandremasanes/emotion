import {AuthGuard} from "./AuthGuard";
import {Type} from "../../model/input/User";
import {Injectable} from "@angular/core";

@Injectable()
export class CustomerAuthGuard extends AuthGuard {

  protected support(): Type {
    return Type.CUSTOMER;
  }
}
