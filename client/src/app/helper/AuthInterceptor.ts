import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {TokenHolder} from "./token-holder.service";
import {Observable} from "rxjs/Observable";
import {Injectable} from "@angular/core";
import {Token} from "../../model/input/Token";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private readonly tokenHolder: TokenHolder) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let token: Token;
    let tokenValue: string;

    token = this.tokenHolder.token;
    tokenValue = token ? token.value : null;

    if(tokenValue && !request.url.includes('tokens'))
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${tokenValue}`,
        },
      });

    return next.handle(request);
  }
}
