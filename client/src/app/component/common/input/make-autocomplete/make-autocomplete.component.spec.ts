import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MakeAutocompleteComponent } from './make-autocomplete.component';

describe('MakeAutocompleteComponent', () => {
  let component: MakeAutocompleteComponent;
  let fixture: ComponentFixture<MakeAutocompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MakeAutocompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MakeAutocompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should createCustomer', () => {
    expect(component).toBeTruthy();
  });
});
