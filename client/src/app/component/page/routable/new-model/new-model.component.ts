import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MakeAutocompleteComponent} from "../../../common/input/make-autocomplete/make-autocomplete.component";
import {ModelCreation} from "../../../../../model/output/ModelCreation";
import {StringUtils} from "../../../../../utils/StringUtils";
import {VehicleService} from "../../../../service/vehicle.service";
import {Router} from "@angular/router";
import {Make} from "../../../../../model/input/Make";
import {VehicleType} from "../../../../../model/input/Model";

@Component({
  selector: 'app-new-model',
  templateUrl: './new-model.component.html',
  styleUrls: ['./new-model.component.css']
})
export class NewModelComponent implements OnInit {

  VehicleType: any;

  @Input()
  modelCreation: ModelCreation;

  @ViewChild(MakeAutocompleteComponent)
  makeAutocomplete: MakeAutocompleteComponent;

  constructor(
    private readonly vehicleService: VehicleService,
    private readonly router: Router
  ) {
    this.VehicleType = VehicleType;
    this.modelCreation = {
      makeId: null,
      vehicleType: null,
      code: null,
      name: null,
    };
  }

  ngOnInit() {
  }

  makeChanged(make: Make): void {
    this.modelCreation.makeId = make.id;
  }

  get completed(): boolean {
    return this.modelCreation.makeId != null &&
      !StringUtils.areBlank([
        this.modelCreation.code,
        this.modelCreation.name,
      ]);
  }

  createModel(): void {
    this.vehicleService.createModel(this.modelCreation)
                       .subscribe(
                         model =>
                           this.router.navigateByUrl(`/modele/${model.id}`)
                       );
  }
}
