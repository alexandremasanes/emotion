import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMakesComponent } from './admin-makes.component';

describe('AdminMakesComponent', () => {
  let component: AdminMakesComponent;
  let fixture: ComponentFixture<AdminMakesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminMakesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminMakesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should createCustomer', () => {
    expect(component).toBeTruthy();
  });
});
