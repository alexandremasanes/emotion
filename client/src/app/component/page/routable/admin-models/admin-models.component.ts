import { Component, OnInit } from '@angular/core';
import {VehicleService} from "../../../../service/vehicle.service";
import {Model} from "../../../../../model/input/Model";

@Component({
  selector: 'app-admin-models',
  templateUrl: './admin-models.component.html',
  styleUrls: ['./admin-models.component.css']
})
export class AdminModelsComponent implements OnInit {

  models: Model[];

  selectedIds: number[];

  constructor(private readonly vehicleService: VehicleService) {
    this.vehicleService.getAllModels()
      .subscribe(models => this.models = models);

    this.selectedIds = [];
  }

  ngOnInit() {
  }

  onCheck(event: any, id: number): void {
    let index: number;
    if(event.target.checked)
      this.selectedIds.push(id);
    else if((index = this.selectedIds.indexOf(id)) != -1)
      this.selectedIds.splice(index);
  }

  deleteModels(): void {
    this.vehicleService.deleteModels(this.selectedIds)
                       .subscribe(() => {
                         for(let model of this.models)
                           if(this.selectedIds.indexOf(model.id) != -1)
                             this.models.splice(this.models.indexOf(model));
                         this.selectedIds = [];
                       });
  }
}
