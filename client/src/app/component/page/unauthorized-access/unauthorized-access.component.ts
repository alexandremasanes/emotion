import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from "@angular/router";
import {Token} from "../../../../model/input/Token";
import {Location} from "@angular/common";

@Component({
  selector: 'app-unauthorized-access',
  templateUrl: './unauthorized-access.component.html',
  styleUrls: ['./unauthorized-access.component.css']
})
export class UnauthorizedAccessComponent implements OnInit {

  @Output("tokenCreated")
  eventEmitter: EventEmitter<Token>;

  constructor(
    private readonly router: Router,
    private readonly location: Location
  ) {
    this.eventEmitter = new EventEmitter;
  }

  ngOnInit() {
    jQuery('#exampleModal1').modal({
      show: true
    })
  }

  tokenCreated(token: Token): void {
    let i: number;
    let uriSegment: string[];

    this.eventEmitter.emit(token);

    uriSegment = window.location.href.split('/').slice(3);

    this.router.navigateByUrl('/' + uriSegment.join('/'));

    jQuery('.modal-backdrop').remove();
  }
}
