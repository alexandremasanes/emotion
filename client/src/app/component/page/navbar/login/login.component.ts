import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TokenCreation} from "../../../../../model/output/TokenCreation";
import {UserService} from "../../../../service/user.service";
import {Token} from "../../../../../model/input/Token";
import {StringUtils} from "../../../../../utils/StringUtils";

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css']
})
export class LoginComponent implements OnInit {

  @Input()
  id: number;

  @Input()
  inline: boolean;

  @Input()
  tokenCreation: TokenCreation;

  @Output("tokenCreated")
  eventEmitter: EventEmitter<Token>;

  constructor(private readonly userService: UserService) {
    this.eventEmitter = new EventEmitter;
  }

  ngOnInit() {
    this.tokenCreation = {
      emailAddress: null,
      password: null,
    };
  }

  get completed(): boolean {
    return !StringUtils.areBlank([
      this.tokenCreation.emailAddress,
      this.tokenCreation.password,
    ]);
  }

  createToken(): void {
    this.userService
        .createToken(this.tokenCreation)
        .subscribe(
          token => {
            token.persistent = this.tokenCreation.persistent;
            this.eventEmitter.emit(token);
          }
        );
  }

}
