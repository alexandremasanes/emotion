import {Component, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {Router} from "@angular/router";
import {Make} from "../../../../../model/input/Make";
import {VehicleSearch} from "../../../../../model/output/VehicleSearch";
import {Model} from "../../../../../model/input/Model";
import {MakeAutocompleteComponent} from "../../../common/input/make-autocomplete/make-autocomplete.component";
import {ModelAutocompleteComponent} from "../../../common/input/model-autocomplete/model-autocomplete.component";

@Component({
  selector: 'app-search',
  templateUrl: 'search.component.html',
  styleUrls: ['search.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class SearchComponent implements OnInit {

  minDate: Date;

  @Input()
  search: VehicleSearch;

  @ViewChild(MakeAutocompleteComponent)
  makeAutocomplete: MakeAutocompleteComponent;

  @ViewChild(ModelAutocompleteComponent)
  modelAutocomplete: ModelAutocompleteComponent;

  constructor(
    private readonly router: Router
  ) {
    this.search = {
      modelVehicleType: null,
      modelId: null,
      modelMakeId: null,
      rentalPickupDate: null,
      rentalNumberOfDay: null,
    };
  }

  ngOnInit() {

    const today = new Date;

    today.setDate(today.getDate() + 1);

    this.minDate = new Date(today.getTime());

    this.search = {
      modelVehicleType: null,
      modelId: null,
      modelMakeId: null,
      rentalPickupDate: null,
      rentalNumberOfDay: null,
    };
  }

  redirectToRental(): void {

    if(this.search.rentalNumberOfDay != null)
      this.search.rentalNumberOfDay = Math.floor(this.search.rentalNumberOfDay);

    this.router.navigate(
      ['/vehicules-disponibles'],
      {
        queryParams: this.search
      }
    );
  }

  makeChanged(make: Make): void {
    this.search.modelMakeId = make.id;
    this.modelAutocomplete.model = null;

    if(make)
      this.modelAutocomplete.reload(make.id);
    else
      this.modelAutocomplete.reload();
  }

  modelChanged(model: Model): void {
    if(model) {
      this.search.modelId = model.id;
      this.makeAutocomplete.make = model.make;
    }
  }
}
