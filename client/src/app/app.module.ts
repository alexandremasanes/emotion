import {BrowserModule} from '@angular/platform-browser';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';

import {AppComponent} from './component/app.component';
import {NavbarComponent} from './component/page/navbar/navbar.component';
import {MainComponent} from './component/page/routable/main/main.component';
import {AboutComponent} from './component/page/routable/about/about.component';
import {SearchComponent} from './component/page/navbar/search/search.component';
import {LoginComponent} from './component/page/navbar/login/login.component';
import {RouterModule, Routes} from "@angular/router";
import {AvailableVehiclesComponent} from "./component/page/routable/available-vehicles/available-vehicles.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {UserNavbarComponent} from './component/page/user-navbar/user-navbar.component';
import {UserService} from "./service/user.service";
import {VehicleService} from "./service/vehicle.service";
import {RentalService} from "./service/rental.service";
import {Ng2AutoCompleteModule} from 'ng2-auto-complete';
import { MakeAutocompleteComponent } from './component/common/input/make-autocomplete/make-autocomplete.component';
import { ModelAutocompleteComponent } from './component/common/input/model-autocomplete/model-autocomplete.component';
import { CustomerRentalHomeComponent } from './component/page/routable/customer-rental-home/customer-rental-home.component';
import { RegistrationComponent } from './component/page/navbar/registration/registration.component';
import {TokenHolder} from "./helper/token-holder.service";
import {AuthInterceptor} from "./helper/AuthInterceptor";
import { CustomerDetailsComponent } from './component/page/routable/customer-details/customer-details.component';
import { UnauthorizedAccessComponent } from './component/page/unauthorized-access/unauthorized-access.component';
import { ForbiddenAccessComponent } from './component/page/forbidden-access/forbidden-access.component';
import {CustomerAuthGuard} from "./helper/CustomerAuthGuard";
import {AdminAuthGuard} from "./helper/AdminAuthGuard";
import { ModelsComponent } from './component/page/routable/models/models.component';
import { MakesComponent } from './component/page/routable/makes/makes.component';
import { NewRentalComponent } from './component/page/routable/new-rental/new-rental.component';
import { RentalComponent } from './component/page/routable/rental/rental.component';
import {AuthenticatedAuthGuard} from "./helper/AuthenticatedAuthGuard";
import { NewMakeComponent } from './component/page/routable/new-make/new-make.component';
import { NewModelComponent } from './component/page/routable/new-model/new-model.component';
import { NewVehicleComponent } from './component/page/routable/new-vehicle/new-vehicle.component';
import { AdminMakesComponent } from './component/page/routable/admin-makes/admin-makes.component';
import { AdminModelsComponent } from './component/page/routable/admin-models/admin-models.component';
import { AdminVehiclesComponent } from './component/page/routable/admin-vehicles/admin-vehicles.component';
import { NewAdminComponent } from './component/page/routable/new-admin/new-admin.component';
import { MakeComponent } from './component/page/routable/make/make.component';
import { ModelComponent } from './component/page/routable/model/model.component';
import { VehicleComponent } from './component/page/routable/vehicle/vehicle.component';
import {AdminsComponent} from "./component/page/routable/admins/admins.component";
import {ResponseInterceptor} from "./helper/ResponseInterceptor";
import {AdminCustomersComponent} from "./component/page/routable/admin-customers/admin-customers.component";
import {AdminRentalsComponent} from "./component/page/routable/admin-rentals/admin-rentals.component";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";

const appRoutes: Routes = [
  {
    path: '', component: MainComponent,
  },
  {
    path: 'vehicules-disponibles', component: AvailableVehiclesComponent,
  },
  {
    path: 'a-propos', component: AboutComponent,
  },
  {
    path: 'marques/:type/:typeName', component: MakesComponent,
  },
  {
    path: 'modeles/:type/:typeName', component: ModelsComponent,
  },
  {
    path: 'mes-locations',
    component: CustomerRentalHomeComponent,
    canActivate: [
      CustomerAuthGuard,
    ],
  },
  {
    path: 'mon-compte',
    component: CustomerDetailsComponent,
    canActivate: [
      CustomerAuthGuard,
    ],
  },
  {
    path: 'vehicule/:vehicleId/nouvelle-location',
    component: NewRentalComponent,
    canActivate : [
      CustomerAuthGuard,
    ],
  },
  {
    path: 'location/:rentalId',
    component: RentalComponent,
    canActivate : [
      AuthenticatedAuthGuard,
    ],
  },
  {
    path: 'marque/creer',
    component: NewMakeComponent,
    canActivate : [
      AdminAuthGuard,
    ],
  },
  {
    path: 'marque/liste',
    component: AdminMakesComponent,
    canActivate : [
      AdminAuthGuard,
    ],
  },
  {
    path: 'marque/:id',
    component: MakeComponent,
    canActivate : [
      AdminAuthGuard,
    ],
  },
  {
    path: 'modele/creer',
    component: NewModelComponent,
    canActivate : [
      AdminAuthGuard,
    ],
  },
  {
    path: 'modele/liste',
    component: AdminModelsComponent,
    canActivate : [
      AdminAuthGuard,
    ],
  },
  {
    path: 'modele/:id',
    component: ModelComponent,
    canActivate : [
      AdminAuthGuard,
    ],
  },
  {
    path: 'vehicule/creer',
    component: NewVehicleComponent,
    canActivate : [
      AdminAuthGuard,
    ],
  },
  {
    path: 'vehicule/liste',
    component: AdminVehiclesComponent,
    canActivate : [
      AdminAuthGuard,
    ],
  },
  {
    path: 'location/liste',
    component: AdminRentalsComponent,
    canActivate : [
      AdminAuthGuard,
    ],
  },
  {
    path: 'location/:id',
    component: RentalComponent,
    canActivate : [
      AdminAuthGuard,
    ],
  },
  {
    path: 'client/liste',
    component: AdminCustomersComponent,
    canActivate : [
      AdminAuthGuard,
    ],
  },
  {
    path: 'client/:id',
    component: CustomerDetailsComponent,
    canActivate : [
      AdminAuthGuard,
    ],
  },
  {
    path: 'admin/creer',
    component: NewAdminComponent,
    canActivate : [
      AdminAuthGuard,
    ],
  },
  {
    path: 'admin/liste',
    component: AdminsComponent,
    canActivate : [
      AdminAuthGuard,
    ],
  },
  {
    path: '401',
    component: UnauthorizedAccessComponent,
  },
  {
    path: '403',
    component: ForbiddenAccessComponent,
  },
  //{ path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MainComponent,
    AboutComponent,
    SearchComponent,
    LoginComponent,
    AvailableVehiclesComponent,
    UserNavbarComponent,
    MakeAutocompleteComponent,
    ModelAutocompleteComponent,
    CustomerRentalHomeComponent,
    RegistrationComponent,
    CustomerDetailsComponent,
    UnauthorizedAccessComponent,
    ForbiddenAccessComponent,
    ModelsComponent,
    MakesComponent,
    NewRentalComponent,
    RentalComponent,
    NewMakeComponent,
    NewModelComponent,
    NewVehicleComponent,
    AdminMakesComponent,
    AdminModelsComponent,
    AdminVehiclesComponent,
    AdminRentalsComponent,
    AdminCustomersComponent,
    AdminsComponent,
    NewAdminComponent,
    MakeComponent,
    ModelComponent,
    VehicleComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: true // <-- debugging purposes only
      }
    ),
    Ng2AutoCompleteModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (
          http =>
            new TranslateHttpLoader(http, './assets/i18n/', '.json')
        ),
        deps: [HttpClient]
      }
    }),
  ],
  providers: [
    UserService,
    VehicleService,
    RentalService,
    TokenHolder,
    CustomerAuthGuard,
    AdminAuthGuard,
    AuthenticatedAuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ResponseInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
