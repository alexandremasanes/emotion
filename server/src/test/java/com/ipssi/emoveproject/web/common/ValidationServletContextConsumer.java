package com.ipssi.emoveproject.web.common;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@ContextConfiguration({
        "classpath*:applicationContext.xml",
        "classpath*:validation-servlet.xml",
})
public interface ValidationServletContextConsumer {
}
