package com.ipssi.emoveproject.web.api.http.handler.resource;

import com.ipssi.emoveproject.web.api.model.input.*;
import com.ipssi.emoveproject.web.common.ApiServletContextConsumer;
import com.ipssi.emoveproject.web.common.http.handler.ControllerTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.albema.common.util.ObjectUtils.toMap;
import static java.math.BigInteger.*;
import static java.time.LocalDate.*;
import static java.util.Locale.FRANCE;
import static org.junit.Assert.assertEquals;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CustomerControllerTest extends ControllerTest implements ApiServletContextConsumer {

    @Value("${mailbox}")
    private String mailbox;

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void should_return_200_when_onGet() throws Exception {
       getMockMvc().perform(get("/customers"))
                   .andExpect(status().isOk());

       getMockMvc().perform(get("/customers/2"))
                    .andExpect(status().isOk());

       getMockMvc().perform(get("/customers").param("ids", "1", "2"))
                   .andExpect(status().isOk());
    }

    @Test
    @WithAnonymousUser
    public void should_return_201_and_retrieve_one_mail_when_onPost() throws Exception {
        final CustomerCreation customerCreation;
        final RestTemplate     restTemplate;

        customerCreation = new CustomerCreation(
                "john-doe@foo.bar", "secret",
                "Doe","John",
                of(1993, 1, 1),
                "1 rue de la Paix", "Paris", "75007"
        );

        restTemplate = new RestTemplate();

        restTemplate.delete("http://" + mailbox + "/email/all");

        getMockMvc().perform(
                post("/customers").contentType(APPLICATION_JSON)
                        .content(
                                gson.toJson(customerCreation)
                        ).locale(FRANCE)
        ).andExpect(
                status().isCreated()
        );

        assertEquals(
                1,
                restTemplate.getForObject("http://" + mailbox + "/email", List.class)
                            .size()
        );
    }

    @Test
    @WithMockUser(username = "2", authorities = "CUSTOMER")
    public void should_return_200_onRentalGet() throws Exception {
        getMockMvc().perform(get("/customers/2/rentals"))
                .andExpect(status().isOk());

        getMockMvc().perform(get("/customers/2/rentals/2"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "2", authorities = "CUSTOMER")
    public void should_return_201_onRentalPost() throws Exception {
        final RentalCreation rentalCreation;

        rentalCreation = new RentalCreation(
                ONE, valueOf(2L), now().plusDays(5), (byte) 2, false
        );

        getMockMvc().perform(
                post("/customers/2/rentals").contentType(APPLICATION_JSON)
                        .content(
                                gson.toJson(rentalCreation)
                        )
        ).andExpect(
                status().isCreated()
        );
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void should_return_status_204_when_onPatch() throws Exception {

        final Map<String, String> customerUpdateMap;

        customerUpdateMap = new HashMap<>(2);

        toMap(
                new GDPRUpdate()
        ).forEach(
                (k, v) -> customerUpdateMap.put(k, v.toString())
        );

        customerUpdateMap.put("__type__", GDPRUpdate.class.getSimpleName());

        getMockMvc().perform(
                patch("/customers/2").contentType(APPLICATION_JSON)
                        .content(
                                gson.toJson(customerUpdateMap)
                        )
        ).andExpect(
                status().isNoContent()
        );

        customerUpdateMap.clear();

        toMap(
                new CustomerDetailsUpdate(
                        "newAddress",
                        "newCity",
                        "newZipCode"
                )
        ).forEach((k, v) -> customerUpdateMap.put(k, v.toString()));

        customerUpdateMap.put("__type__", CustomerDetailsUpdate.class.getSimpleName());

        getMockMvc().perform(
                patch("/customers/2").contentType(APPLICATION_JSON)
                        .content(
                                gson.toJson(customerUpdateMap)
                        )
        ).andExpect(
                status().isNoContent()
        );
    }
}