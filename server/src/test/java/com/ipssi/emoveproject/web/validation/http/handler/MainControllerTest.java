package com.ipssi.emoveproject.web.validation.http.handler;

import com.ipssi.emoveproject.web.common.ValidationServletContextConsumer;
import com.ipssi.emoveproject.web.common.http.handler.ControllerTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class MainControllerTest extends ControllerTest implements ValidationServletContextConsumer {

    @Value("${app.server}")
    private String appServer;

    @Test
    public void should_return_303_and_appServer_when_onPost() throws Exception {

        getMockMvc().perform(
                post("/1").param(
                        "validationCode",
                        "850697e480b2480961f963045f23333f"
                )
        ).andExpect(status().isSeeOther())
         .andExpect(header().string("Location", "http://" + appServer + "/?validation"));
    }
}