package com.ipssi.emoveproject.web.api.http.handler.resource;

import com.ipssi.emoveproject.web.api.model.input.TokenCreation;
import com.ipssi.emoveproject.web.common.ApiServletContextConsumer;
import com.ipssi.emoveproject.web.common.http.handler.ControllerTest;
import org.junit.Test;
import org.springframework.security.test.context.support.WithAnonymousUser;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TokenControllerTest extends ControllerTest implements ApiServletContextConsumer {

    @Test
    @WithAnonymousUser
    public void should_return_status_200_when_on_authentication() throws Exception {
        getMockMvc().perform(
                post("/tokens").contentType(APPLICATION_JSON)
                        .content(
                                gson.toJson(new TokenCreation("test@test.test", "test"))
                        )
        ).andExpect(status().isCreated());
    }
}
