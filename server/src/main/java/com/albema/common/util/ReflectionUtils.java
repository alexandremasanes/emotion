package com.albema.common.util;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static java.io.File.separator;
import static java.lang.Class.*;
import static java.lang.Package.getPackage;
import static java.lang.String.format;
import static java.lang.String.valueOf;
import static java.lang.Thread.currentThread;
import static java.net.URLDecoder.decode;
import static java.nio.charset.Charset.defaultCharset;
import static org.apache.commons.io.FileUtils.EMPTY_FILE_ARRAY;
import static org.apache.commons.lang3.ArrayUtils.contains;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;

public final class ReflectionUtils {

    private static final char   PKG_SEPARATOR = '.';

    private static final String CLASS_FILE_SUFFIX = ".class";

    private static final String BAD_PACKAGE_ERROR_MSG_FORMAT =
            "Unable to get resources from path '%s'. " +
            "Are you sure the package '%s' exists?";

    private ReflectionUtils() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Utility class.");
    }

    public static List<Class<?>> findClasses(
            final Package     targetPackage,
            final Class       subTypeOf,
            final Class<?>... ignored
    ) throws UnsupportedEncodingException {
        final String scannedPath;
        final URL scannedUrl;
        final File scannedDir;
        final List<Class<?>> classes;

        scannedPath = targetPackage.getName()
                                   .replace(valueOf(PKG_SEPARATOR), separator);

        scannedUrl = currentThread().getContextClassLoader()
                                    .getResource(scannedPath);

        if(scannedUrl == null)
            throw new IllegalArgumentException(
                    format(
                            BAD_PACKAGE_ERROR_MSG_FORMAT,
                            scannedPath,
                            targetPackage.toString()
                    )
            );

        scannedDir = new File(
                decode(
                    scannedUrl.getPath(),
                    valueOf(defaultCharset())
                )
        );

        classes = new ArrayList<>();

        for(final File file : defaultIfNull(scannedDir.listFiles(), EMPTY_FILE_ARRAY))
            classes.addAll(findClasses(file, targetPackage, subTypeOf, ignored));

        return classes;
    }

    public static List<Class<?>> findClasses(
            final Package     targetPackage,
            final Class<?>... ignored
    ) throws UnsupportedEncodingException {
        return findClasses(targetPackage, Object.class, ignored);
    }

    private static List<Class<?>> findClasses(
            final File       file,
            final Package    targetPackage,
            final Class      subTypeOf,
            final Class<?>[] ignored
    ) {
        final List<Class<?>> classes;
        final String resource;
        final int endIndex;
        final String className;
        final Class<?> clazz;

        classes = new ArrayList<>();
        resource = targetPackage.getName() + PKG_SEPARATOR + file.getName();

        if(file.isDirectory())
            for(final File child : defaultIfNull(file.listFiles(), EMPTY_FILE_ARRAY))
                classes.addAll(findClasses(child, getPackage(resource), subTypeOf, ignored));
        else if (resource.endsWith(CLASS_FILE_SUFFIX)) {
            endIndex = resource.length() - CLASS_FILE_SUFFIX.length();
            className = resource.substring(0, endIndex);
            try {
                clazz = forName(className);
                if(!contains(ignored, clazz) && subTypeOf.isAssignableFrom(clazz))
                    classes.add(clazz);
            } catch (final ClassNotFoundException e) {}
        }

        return classes;
    }
}
