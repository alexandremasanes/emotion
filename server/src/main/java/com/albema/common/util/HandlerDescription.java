package com.albema.common.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;

@SuppressWarnings("unused")
public final class HandlerDescription {

    private final List<Map.Entry<String, Annotation>> annotedParameters;

    private final Method method;

    public HandlerDescription(Method method) {
        this.method = method;
        annotedParameters = new ArrayList<>();
    }

    public List<Map.Entry<String, Annotation>> getAnnotedParameters() {
        return annotedParameters;
    }

    public void addAnnotedParameter(String key, Annotation annotation) {
        annotedParameters.add(new AbstractMap.SimpleEntry<>(key, annotation));
    }

    public Method getMethod() {
        return method;
    }
}