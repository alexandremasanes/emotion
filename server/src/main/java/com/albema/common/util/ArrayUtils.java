package com.albema.common.util;


import static java.util.Arrays.copyOfRange;

@SuppressWarnings("WeakerAccess")
public final class ArrayUtils {

    private ArrayUtils() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Utility class.");
    }

    static
    public <T> T[] unshift(T[] array, T value) {
        array = copyOfRange(array, -1, array.length);
        array[0] = value;
        return array;
    }
}