package com.albema.common.orm.entry.manytoone;

import com.albema.common.orm.entity.BaseEntity;
import com.albema.common.orm.entry.BaseEntry;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@SuppressWarnings("unused")
@Embeddable
public class Entry<K extends BaseEntity, V extends Serializable> extends BaseEntry<K, V> {

    private static final long serialVersionUID = -2745140778620998369L;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn
    private K key;

    public Entry(K key) {
        this.key = key;
    }

    protected Entry() {
    }

    @Override
    public K getKey() {
        return key;
    }
}
