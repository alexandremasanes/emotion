package com.albema.common.orm.entity;

import java.io.Serializable;
import java.lang.reflect.Proxy;

@SuppressWarnings("all")
public abstract class BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    protected static void check(boolean bool) {
        if (!bool)
            throw new BusinessException();
    }

    @SuppressWarnings("unchecked")
    public static Class<? extends BaseEntity> resolvedClass(BaseEntity entity) {
        return Proxy.isProxyClass(entity.getClass()) ? (Class<? extends BaseEntity>) entity.getClass().getSuperclass() : entity.getClass();
    }

    @Override
    public int hashCode() {
        throw new UnsupportedOperationException("Object#hashCode must be overridden !");
    }

    @Override
    public boolean equals(Object that) {
        throw new UnsupportedOperationException("Object#equals must be overridden !");
    }

    public static class BusinessException extends RuntimeException {
    }
}