package com.ipssi.emoveproject.web.files.http.handler;

import com.ipssi.emoveproject.web.files.helper.FileManager;
import com.ipssi.emoveproject.web.files.model.input.validation.ValidFile;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

import java.io.IOException;

import static org.springframework.http.HttpStatus.CREATED;

@RequiredArgsConstructor
@RestController("FilesMainController")
class MainController {

    private final FileManager fileManager;

    @PostMapping("/{directory:makes|models|vehicles}/{id}")
    @ResponseStatus(CREATED)
    @PreAuthorize("hasAuthority('ADMIN')")
    public String onPost(
            @PathVariable String directory,
            @PathVariable String id,
            @Valid
            @ValidFile
            @RequestParam MultipartFile file
    ) throws IOException {
        return fileManager.save(directory,  id, file);
    }
}
