package com.ipssi.emoveproject.web.api.http.handler.resource;

import com.ipssi.emoveproject.business.logic.VehicleService;
import com.ipssi.emoveproject.business.mapping.VehicleType;
import com.ipssi.emoveproject.web.api.model.input.ModelCreation;
import com.ipssi.emoveproject.web.api.model.output.ModelDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.math.BigInteger;
import java.util.List;

import static com.albema.common.util.ObjectUtils.ifNotNull;
import static com.ipssi.emoveproject.web.api.model.output.DTO.fromCollection;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RequiredArgsConstructor
@RestController
@RequestMapping("/models")
class ModelController {

    private final VehicleService vehicleService;

    @GetMapping
    public List<ModelDTO> onGet() {
        return fromCollection(vehicleService.getAllModels(), ModelDTO::new);
    }

    @GetMapping(params = "ids")
    public List<ModelDTO> onGet(@RequestParam BigInteger[] ids) {
        return fromCollection(vehicleService.getModels(ids), ModelDTO::new);
    }

    @GetMapping("/{id}")
    public ModelDTO onGet(@PathVariable BigInteger id) {
        return ifNotNull(vehicleService.getOneModel(id), ModelDTO::new);
    }

    @GetMapping(params = "name")
    public List<ModelDTO> onGet(@RequestParam String name) {
        return fromCollection(
                vehicleService.getModels(name),
                ModelDTO::new
        );
    }

    @GetMapping(params = {"makeId", "name"})
    public List<ModelDTO> onGet(
            @RequestParam BigInteger makeId,
            @RequestParam String     name
    ) {
        return fromCollection(
                vehicleService.getModels(makeId, name),
                ModelDTO::new
        );
    }

    @GetMapping(params = "vehicleType")
    public List<ModelDTO> onGet(
            @RequestParam VehicleType vehicleType
    ) {
        return fromCollection(
          vehicleService.getModels(vehicleType),
          ModelDTO::new
        );
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @ResponseStatus(CREATED)
    @PostMapping
    public ModelDTO onPost(@Valid @RequestBody ModelCreation creation) {
        return new ModelDTO(vehicleService.createModel(creation));
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @ResponseStatus(NO_CONTENT)
    @PatchMapping("/{id}")
    public void onPatch(
            @PathVariable                 BigInteger id,
            @Valid @NotBlank @RequestBody String     newName
    ) {
        vehicleService.updateModel(id, newName);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @ResponseStatus(NO_CONTENT)
    @DeleteMapping("/{id}")
    public void onDelete(@PathVariable BigInteger id) {
        vehicleService.deleteModels(id);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @ResponseStatus(NO_CONTENT)
    @DeleteMapping(params = "ids")
    public void onDelete(@RequestParam BigInteger[] ids) {
        vehicleService.deleteModels(ids);
    }
}
