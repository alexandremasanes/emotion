package com.ipssi.emoveproject.web.validation.model.input.validation;

import com.albema.spring.validation.BusinessDataValidator;
import com.ipssi.emoveproject.business.mapping.Customer;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintValidatorContext;
import java.io.Serializable;

@Component
class ValidationCodeValidator extends BusinessDataValidator<ValidationCode> {

    @Override
    @Transactional(readOnly = true)
    public boolean isValid(Serializable serializable, ConstraintValidatorContext constraintValidatorContext) {
        final String stm;

        stm = "SELECT CASE COUNT(*) WHEN 0 THEN FALSE ELSE TRUE END " +
              "FROM " + Customer.class.getName() + " " +
              "WHERE FUNCTION(" +
                "'MD5'," +
                "emailAddress || firstName || CAST(birthDate AS String)" +
              ") = :serializable";

        return getCurrentSession().createQuery(stm, Boolean.class)
                                  .setParameter("serializable", serializable)
                                  .uniqueResult();
    }

    ValidationCodeValidator(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
