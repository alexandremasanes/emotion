package com.ipssi.emoveproject.web.validation.http.handler;

import com.ipssi.emoveproject.business.logic.UserService;
import com.ipssi.emoveproject.web.validation.model.input.validation.ValidationCode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.math.BigInteger;

import static javax.servlet.http.HttpServletResponse.SC_SEE_OTHER;
import static org.springframework.http.HttpHeaders.*;

@Controller("ValidationMainController")
class MainController {

    private final String      redirectUri;

    private final UserService userService;

    @PostMapping("/{id}")
    public void onPost(
            @PathVariable                        BigInteger          id,
            @SuppressWarnings("unused")
            @Valid @ValidationCode @RequestParam String              validationCode,
                                                 HttpServletResponse response
    ) {

        userService.updateCustomer(id,true);

        response.setStatus(SC_SEE_OTHER);
        response.setHeader(
                LOCATION,
                "http://" + redirectUri + "/?validation"
        );
    }

    MainController(
            @Value("${app.server}") String      redirectUri,
                                    UserService userService
    ) {
        this.redirectUri = redirectUri;
        this.userService = userService;
    }
}
