package com.ipssi.emoveproject.web.api.model.input.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = MajorityValidator.class)
@Target({
        ElementType.FIELD,
        ElementType.METHOD,
})
@Retention(RetentionPolicy.RUNTIME)
public @interface Majority {

    String message() default "Majority";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
