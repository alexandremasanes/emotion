package com.ipssi.emoveproject.web.api.http.security;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static javax.servlet.http.HttpServletResponse.*;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.security.core.authority.AuthorityUtils.createAuthorityList;

@Component
class JwtAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    @SuppressWarnings("FieldCanBeLocal")
    private static String ANONYMOUS = "ANONYMOUS";

    JwtAuthenticationFilter(
            AuthenticationManager        authenticationManager,
            AuthenticationSuccessHandler authenticationSuccessHandler
    ) {
        super("/**");
        setAuthenticationManager(authenticationManager);
        setAuthenticationSuccessHandler(authenticationSuccessHandler);
    }

    @Override
    protected boolean requiresAuthentication(
            HttpServletRequest  request,
            HttpServletResponse response
    ) {
        return true;
    }

    @Override
    public Authentication attemptAuthentication(
            HttpServletRequest  request,
            HttpServletResponse response
    ) throws AuthenticationException {
        final String                 header;
        final String                 authToken;
        final JwtAuthenticationToken authRequest;

        header = request.getHeader(AUTHORIZATION);

        if(header == null) {
            return new AnonymousAuthenticationToken(ANONYMOUS, ANONYMOUS, createAuthorityList(ANONYMOUS));
        } else if(!header.startsWith("Bearer ")) {
            response.setStatus(SC_BAD_REQUEST);
            return null;
        }

        authToken = header.substring(7);

        authRequest = new JwtAuthenticationToken(authToken);

        return getAuthenticationManager().authenticate(authRequest);
    }

    @Override
    protected void successfulAuthentication(
            HttpServletRequest  request,
            HttpServletResponse response,
            FilterChain         chain,
            Authentication      authentication
    )
            throws IOException, ServletException {

        super.successfulAuthentication(
                request, response, chain, authentication
        );

        chain.doFilter(request, response);
    }
}
