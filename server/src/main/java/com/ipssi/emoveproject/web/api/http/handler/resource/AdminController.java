package com.ipssi.emoveproject.web.api.http.handler.resource;

import com.ipssi.emoveproject.business.logic.UserService;
import com.ipssi.emoveproject.web.api.model.input.AdminCreation;
import com.ipssi.emoveproject.web.api.model.output.AdminDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import static com.albema.common.util.ObjectUtils.ifNotNull;
import static com.ipssi.emoveproject.web.api.model.output.DTO.fromCollection;
import static org.springframework.http.HttpStatus.CREATED;

@RequiredArgsConstructor
@PreAuthorize("hasAuthority('ADMIN')")
@RestController
@RequestMapping("/admins")
class AdminController {

    private final UserService userService;

    @GetMapping
    public List<AdminDTO> onGet() {
        return fromCollection(userService.getAllAdmins(), AdminDTO::new);
    }

    @GetMapping(params = "ids")
    public List<AdminDTO> onGet(@RequestParam BigInteger[] ids) {
        return fromCollection(userService.getAdmins(ids), AdminDTO::new);
    }

    @GetMapping("/{id}")
    public AdminDTO onGet(@PathVariable BigInteger id) {
        return ifNotNull(userService.getOneAdmin(id), AdminDTO::new);
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public AdminDTO onPost(
            @RequestBody AdminCreation adminCreation
    ) throws NoSuchAlgorithmException {
        return new AdminDTO(userService.createAdmin(adminCreation));
    }
}
