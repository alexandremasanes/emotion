package com.ipssi.emoveproject.web.api.http.handler.resource;

import com.ipssi.emoveproject.business.logic.VehicleService;
import com.ipssi.emoveproject.web.api.model.input.VehicleCreation;
import com.ipssi.emoveproject.web.api.model.input.VehicleSearch;
import com.ipssi.emoveproject.web.api.model.input.VehicleUpdate;
import com.ipssi.emoveproject.web.api.model.output.VehicleDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigInteger;
import java.util.List;

import static com.albema.common.util.ObjectUtils.*;
import static com.ipssi.emoveproject.web.api.model.output.DTO.fromCollection;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RequiredArgsConstructor
@RestController
@RequestMapping("/vehicles")
class VehicleController {

    private final VehicleService vehicleService;

    @GetMapping
    public List<VehicleDTO> onGet() {
        return fromCollection(vehicleService.getAll(), VehicleDTO::new);
    }

    @GetMapping(params = "search")
    public List<VehicleDTO> onGet(VehicleSearch vehicleSearch) {
        return fromCollection(
                vehicleService.get(vehicleSearch),
                VehicleDTO::new
        );
    }

    @GetMapping(params = "ids")
    public List<VehicleDTO> onGet(@RequestParam BigInteger[] ids) {
        return fromCollection(vehicleService.get(ids), VehicleDTO::new);
    }

    @GetMapping("/{id}")
    public VehicleDTO onGet(@PathVariable BigInteger id) {
        return ifNotNull(vehicleService.getOne(id), VehicleDTO::new);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @ResponseStatus(CREATED)
    @PostMapping
    public VehicleDTO onPost(@Valid @RequestBody VehicleCreation creation) {
        return new VehicleDTO(vehicleService.create(creation));
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @ResponseStatus(NO_CONTENT)
    @PatchMapping("/{id}")
    public void onPatch(@PathVariable BigInteger id, @RequestBody VehicleUpdate update) {
        vehicleService.update(id, update);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @ResponseStatus(NO_CONTENT)
    @DeleteMapping("/{id}")
    public void onDelete(@PathVariable BigInteger id) {
        vehicleService.delete(id);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @ResponseStatus(NO_CONTENT)
    @DeleteMapping(params = "ids")
    public void onDelete(@RequestParam BigInteger[] ids) {
        vehicleService.delete(ids);
    }
}
