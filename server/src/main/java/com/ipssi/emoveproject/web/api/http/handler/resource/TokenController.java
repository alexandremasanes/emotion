package com.ipssi.emoveproject.web.api.http.handler.resource;

import com.ipssi.emoveproject.business.logic.UserService;
import com.ipssi.emoveproject.business.mapping.User;
import com.ipssi.emoveproject.web.api.http.security.JwtManager;
import com.ipssi.emoveproject.web.api.model.input.TokenCreation;
import com.ipssi.emoveproject.web.api.model.output.TokenDTO;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;

import static org.springframework.http.HttpStatus.CREATED;

@RequiredArgsConstructor
@RestController
@RequestMapping("/tokens")
class TokenController {

    private final AuthenticationManager authenticationManager;

    private final JwtManager jwtManager;

    private final UserService userService;

    @PreAuthorize("isAnonymous()")
    @ResponseStatus(CREATED)
    @PostMapping
    public TokenDTO onPost(
            @RequestBody @Valid TokenCreation tokenCreation
    ) throws NoSuchAlgorithmException {
        final Authentication        authentication;
        final User                  user;
        final Pair<String, Instant> tokenAndExpirationTime;

        authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        tokenCreation.getEmailAddress(),
                        tokenCreation.getPassword()
                )
        );

        user = userService.getOne(
                tokenCreation.getEmailAddress(),
                tokenCreation.getPassword()
        );

        tokenAndExpirationTime = jwtManager.generateToken(authentication);

        return new TokenDTO(
                user,
                tokenAndExpirationTime.getLeft(),
                tokenAndExpirationTime.getRight()
        );
    }
}
