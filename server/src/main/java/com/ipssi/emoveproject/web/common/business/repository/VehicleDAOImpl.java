package com.ipssi.emoveproject.web.common.business.repository;

import com.albema.spring.repository.DAO;
import com.ipssi.emoveproject.business.mapping.*;
import com.ipssi.emoveproject.business.repository.VehicleDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

@Repository
class VehicleDAOImpl extends DAO implements VehicleDAO {

    @Override
    public List<Vehicle> find() {
        return find0(Vehicle.class);
    }

    @Override
    public List<Vehicle> find(BigInteger... ids) {
        return find0(Vehicle.class, ids);
    }

    @Override
    public Vehicle find(BigInteger id) {
        return find0(Vehicle.class, id);
    }

    @Override
    public int remove(BigInteger... ids) {
        return remove0(Vehicle.class, ids);
    }

    @Override
    public boolean remove(BigInteger id) {
        return remove0(Vehicle.class, id) != 0;
    }

    @Override
    public boolean remove(Vehicle vehicle) {
        return remove0(vehicle);
    }

    @Override
    public int remove(Vehicle... vehicles) {
        return remove0(vehicles);
    }

    @Override
    public void save(Vehicle entity) {
        save0(entity);
    }

    @Override
    public void save(Vehicle... entities) {
        save0(entities);
    }

    @Override
    public List<Vehicle> find(Search search) {
        final Session                 session;
        final CriteriaBuilder         builder;
        final CriteriaQuery<Vehicle>  criteriaQuery;
        final Root<Vehicle>           root;
        final HashMap<String, Object> parameters;
        final Query<Vehicle>          query;
        final Join<Vehicle, Model>    modelJoin;
        final Join<?, Make>           makeJoin;
        final Join<Vehicle, Rental>   rentalJoin;

        session = getCurrentSession();

        builder = session.getCriteriaBuilder();

        criteriaQuery = builder.createQuery(Vehicle.class);

        root = criteriaQuery.from(Vehicle.class);

        parameters = new HashMap<>(6);

        if(search.getModelId() != null || search.getModelVehicleType() != null) {
            modelJoin = root.join("model");

            if(search.getModelId() != null) {
                modelJoin.on(builder.equal(modelJoin.get("id"), builder.parameter(BigInteger.class, "modelId")));
                parameters.put("modelId", search.getModelId());
            }

            if(search.getModelVehicleType() != null) {
                modelJoin.on(builder.equal(modelJoin.get("vehicleType"), builder.parameter(VehicleType.class, "modelVehicleType")));
                parameters.put("modelVehicleType", search.getModelVehicleType());
            }
        } else
            modelJoin = null;

        if(search.getModelMakeId() != null) {
            if(modelJoin == null)
                makeJoin = root.join("model")
                               .join("make");
            else
                makeJoin = modelJoin.join("make");

            makeJoin.on(builder.equal(makeJoin.get("id"), builder.parameter(BigInteger.class, "modelMakeId")));
            parameters.put("modelMakeId", search.getModelMakeId());
        }

        if(search.getRentalPickupDate() != null && search.getRentalNumberOfDay() != null) {
            criteriaQuery.where(
                    builder.function(
                            "doesRentalOverlaps",
                            Boolean.class,
                            builder.parameter(
                                    LocalDate.class,
                                    "rentalPickupDate"
                            ),
                            builder.parameter(
                                    Short.class,
                                    "rentalNumberOfDay"
                            ),
                            root.get("id")
                    )
            );
        } else if(search.getRentalPickupDate() != null) {
            rentalJoin = root.join("rentals");
            rentalJoin.on(
                    builder.notEqual(
                            rentalJoin.get("pickupDate"),
                            builder.parameter(LocalDate.class, "rentalPickupDate")
                    )
            );
        } else if(search.getRentalNumberOfDay() != null) {
            criteriaQuery.where(
                    builder.equal(
                      builder.function(
                              "isRentalDurationAvailable",
                              Boolean.class,
                              builder.parameter(
                                      Byte.class,
                                      "rentalNumberOfDay"
                              ),
                              root.get("id")

                      ),
                      true
                    )
            );
        }

        if(search.getRentalPickupDate() != null)
            parameters.put("rentalPickupDate", search.getRentalPickupDate());

        if(search.getRentalNumberOfDay() != null)
            parameters.put("rentalNumberOfDay", search.getRentalNumberOfDay());

        query = session.createQuery(criteriaQuery.select(root));
        System.err.println(search);
        System.err.println(query.getQueryString());
        parameters.forEach(query::setParameter);

        return query.list();
    }

    VehicleDAOImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
