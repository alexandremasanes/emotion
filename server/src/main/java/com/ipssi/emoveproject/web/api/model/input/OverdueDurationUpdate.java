package com.ipssi.emoveproject.web.api.model.input;

import com.ipssi.emoveproject.business.logic.RentalService;
import lombok.*;

import javax.validation.constraints.PositiveOrZero;

@Getter
@ToString
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class OverdueDurationUpdate extends RentalUpdate implements RentalService.OverdueDurationUpdate {

    @PositiveOrZero
    private Byte overdueDuration;
}
