package com.ipssi.emoveproject.web.api.http.handler.resource;

import com.ipssi.emoveproject.business.logic.RentalService;
import com.ipssi.emoveproject.web.api.model.input.RentalUpdate;
import com.ipssi.emoveproject.web.api.model.output.RentalDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigInteger;
import java.util.List;

import static com.albema.common.util.ObjectUtils.ifNotNull;
import static com.ipssi.emoveproject.web.api.model.output.DTO.fromCollection;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RequiredArgsConstructor
@PreAuthorize("hasAuthority('ADMIN')")
@RestController
@RequestMapping("/rentals")
class RentalController {

    private final RentalService rentalService;

    @GetMapping
    public List<RentalDTO> onGet() {
        return fromCollection(rentalService.getAll(), RentalDTO::new);
    }

    @GetMapping(params = "id")
    public List<RentalDTO> onGet(@RequestParam BigInteger[] ids) {
        return fromCollection(rentalService.get(ids), RentalDTO::new);
    }

    @GetMapping("/{id}")
    public RentalDTO onGet(@PathVariable BigInteger id) {
        return ifNotNull(rentalService.getOne(id), RentalDTO::new);
    }

    @ResponseStatus(NO_CONTENT)
    @PatchMapping("/{id}")
    public void onPatch(@PathVariable BigInteger id, @Valid @RequestBody RentalUpdate update) {
        rentalService.update(id, update);
    }

    @ResponseStatus(NO_CONTENT)
    @DeleteMapping("/{id}")
    public void onDelete(@PathVariable BigInteger id) {
        rentalService.delete(id);
    }


    @ResponseStatus(NO_CONTENT)
    @DeleteMapping(params = "ids")
    public void onDelete(@RequestParam BigInteger[] ids) {
        rentalService.delete(ids);
    }
}
