package com.ipssi.emoveproject.business.mapping;

import com.albema.common.orm.entity.RecordableEntity;
import com.albema.common.orm.entity.identifiable.IdentifiableById;
import lombok.*;

import javax.persistence.*;
import java.math.BigInteger;

import static javax.persistence.GenerationType.*;
import static javax.persistence.InheritanceType.*;
import static lombok.AccessLevel.*;

@Getter
@Entity
@Table(name = "users")
@Inheritance(strategy = JOINED)
@NoArgsConstructor(access = PROTECTED)
@EqualsAndHashCode(of = "emailAddress", callSuper = false)
public abstract class User extends RecordableEntity implements IdentifiableById {

    @Id
    @GeneratedValue(
            strategy  = SEQUENCE,
            generator = "userIdSeqGen"
    )
    @SequenceGenerator(
            name = "userIdSeqGen",
            sequenceName = "seq_users"
    )
    private BigInteger id;

    @Basic(optional = false)
    @Column(
            name = "email_address",
            nullable = false,
            unique = true
    )
    private String emailAddress;

    @Setter
    private String hash;

    public User(@NonNull String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
