package com.ipssi.emoveproject.business.repository;

import com.albema.common.orm.repository.Finder;
import com.albema.common.orm.repository.Remover;
import com.albema.common.orm.repository.Saver;
import com.ipssi.emoveproject.business.mapping.Vehicle;
import com.ipssi.emoveproject.business.mapping.VehicleType;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

public interface VehicleDAO extends Finder<Vehicle>, Saver<Vehicle>, Remover<Vehicle> {

    interface Search {

        VehicleType getModelVehicleType();

        BigInteger  getModelId();

        BigInteger  getModelMakeId();

        LocalDate   getRentalPickupDate();

        Byte        getRentalNumberOfDay();
    }

    List<Vehicle> find(Search search);
}
