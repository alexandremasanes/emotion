package com.ipssi.emoveproject.business.mapping;

import com.albema.common.orm.entity.RecordableEntity;
import com.albema.common.orm.entity.identifiable.IdentifiableById;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.Validate;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

@Entity
@Table(name = "makes")
@NoArgsConstructor(access = PROTECTED)
@EqualsAndHashCode(of = "code", callSuper = false)
public class Make extends RecordableEntity implements IdentifiableById {

    @Getter
    @Id
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "makeSeqGen"
    )
    @SequenceGenerator(
            name = "makeSeqGen",
            sequenceName = "seq_makes"
    )
    private BigInteger id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Basic(optional = false)
    @Column(nullable = false, unique = true)
    private String code;

    @OneToMany(
            mappedBy = "make",
            cascade = ALL,
            orphanRemoval = true
    )
    private Set<Model> models;

    {
        models = new HashSet<>(0);
    }

    public Make(@NonNull String code) {
        this.code = Validate.notBlank(code, "Code cannot be blank");
    }

    @NonNull
    public Set<Model> getModels() {
        return new HashSet<>(models);
    }

    boolean addModel(@lombok.NonNull @NonNull Model model) {
        return models.add(model);
    }
}
